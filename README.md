Puppet SE Demo Environment
==========================

This is the control-repo for Puppetize Live 2018.

* [Consume](docs/consume.md)
* [Contribute](docs/contribute.md)
