# Modules from the Puppet Forge
# Core modules on which many others depend
mod 'puppetlabs/stdlib',     '5.1.0'
mod 'puppetlabs/chocolatey', '3.0.0'
mod 'puppetlabs/concat',     '4.0.0'
mod 'puppet/staging',        '3.2.0'

# Versions should be updated to be the latest at the time you start
mod 'puppetlabs-cd4pe',                '1.0.2'
mod 'puppetlabs-puppet_authorization', '0.4.0'
mod 'puppetlabs/bolt_shim',            '0.1.1'
mod 'puppetlabs/exec',                 '0.3.0'
mod 'puppetlabs/resource',             '0.1.0'
mod 'puppetlabs/service',              '0.4.0'
mod 'puppetlabs/inifile',              '2.4.0'
mod 'puppetlabs/ciscopuppet',          '1.10.0'
mod 'puppetlabs/acl',                  '2.0.1'
mod 'puppetlabs/apache',               '3.4.0'
mod 'puppetlabs/aws',                  '2.1.0'
mod 'puppetlabs/azure',                '1.3.1'
mod 'puppetlabs/azure_arm',            '0.1.4'
mod 'puppetlabs/dism',                 '1.2.0'
mod 'puppetlabs/dsc',                  '1.6.0'
mod 'puppetlabs/dsc_lite',             '1.0.0'
mod 'puppetlabs/firewall',             '1.14.0'
mod 'puppetlabs/git',                  '0.5.0'
mod 'puppetlabs/haproxy',              '2.2.0'
mod 'puppetlabs/iis',                  '4.4.0'
mod 'puppetlabs/java',                 '3.2.0'
mod 'puppetlabs/limits',               '0.1.0'
mod 'puppetlabs/motd',                 '2.1.1'
mod 'puppetlabs/mount_iso',            '2.0.0'
mod 'puppetlabs/mysql',                '6.2.0'
mod 'puppetlabs/netdev_stdlib',        '0.16.0'
mod 'puppetlabs/ntp',                  '7.3.0'
mod 'puppetlabs/powershell',           '2.1.5'
mod 'puppetlabs/puppet_agent',         '1.7.0'
mod 'puppetlabs/puppetserver_gem',     '1.0.0'
mod 'puppetlabs/reboot',               '2.0.0'
mod 'puppetlabs/registry',             '2.0.2'
mod 'puppetlabs/tomcat',               '2.4.0'
mod 'puppetlabs/vcsrepo',              '2.4.0'
mod 'puppetlabs/sqlserver',            '2.1.1'
mod 'puppetlabs/apt',                  '6.1.1'
mod 'puppetlabs/facter_task',          '0.4.0'
mod 'puppetlabs/docker',               '3.0.0'
mod 'puppetlabs/transition',           '0.1.1'
mod 'puppetlabs/distelli_agent',       '0.1.0'

# Forge Puppet Vox Pupuli Modules
mod 'puppet-archive',          '3.2.0'
mod 'puppet-hiera',            '3.3.3'
mod 'puppet-splunk',           '7.2.1'
mod 'puppet-windows_env',      '3.1.1'
mod 'puppet-windows_firewall', '2.0.1'
mod 'puppet-windowsfeature',   '3.2.1'
mod 'puppet-python',           '2.1.1'
mod 'puppet-php',              '6.0.0'
mod 'puppet-nginx',            '0.13.0'
mod 'puppet-selinux',          '1.6.0'

# Forge Community Modules
mod 'saz-sudo',                          '5.0.0'
mod 'tspy-code_deploy',                  '1.0.2'
mod 'abuxton-pdk',                       '0.0.2'
mod 'beersy-hiera_eyaml_setup',          '0.1.2'
mod 'beersy-pe_code_manager_easy_setup', '2.0.3'
mod 'npwalker-pe_code_manager_webhook',  '2.0.1'
mod 'pltraining-rbac',                   '0.0.7'
mod 'abrader-gms',                       '1.0.3'
mod 'geoffwilliams-chown_r',             '1.0.1'
mod 'ghoneycutt-ssh',                    '3.58.0'
mod 'ipcrm/echo',                        '0.1.3'
mod 'stahnma/epel',                      '1.3.1'
mod 'aristanetworks-eos',                '1.5.0'
mod 'aristanetworks-netdev_stdlib_eos',  '1.2.0'
mod 'WhatsARanjit/node_manager',         '0.7.1'
mod 'cyberious/pget',                    '1.1.0'
mod 'lwf/remote_file',                   '1.1.3'
mod 'ajjahn/samba',                      '0.5.0'
mod 'ayohrling-local_security_policy',   '0.6.3'
mod 'thias-sysctl',                      '1.0.6'
mod 'reidmv/unzip',                      '0.1.2'
mod 'andulla/vsphere_conf',              '0.0.9'
mod 'biemond/wildfly',                   '2.3.2'
mod 'cyberious/windows_java',            '1.0.2'
mod 'hunner/wordpress',                  '1.0.0'
mod 'tse/winntp',                        '1.0.1'
mod 'tse/time',                          '1.0.1'
mod 'jriviere/windows_ad',               '0.3.2'
mod 'crayfishx-purge',                   '1.2.1'
mod 'jpadams-puppet_vim_env',            '2.4.1'
mod 'bodgit-rngd',                       '2.0.2'
mod 'autostructure-auditpol',            '1.0.0'
mod 'nexcess-auditd',                    '2.0.0'
mod 'jdowning-rbenv',                    '2.4.0'
mod 'camptocamp-systemd',                '2.1.0'

#mod 'gogs',
#  :git => 'https://github.com/ipcrm/puppet-gogs.git',
#  :ref => '59f7800ad3512cf371c47902996df0b927267805'

#mod 'puppet_module',
#  :git => 'https://github.com/tspeigner/puppet_module.git'

#mod 'tse-tse_facts',
#  :git => 'https://github.com/puppetlabs/tse-module-tse_facts.git'

#mod 'demo_cis',
#  :git => 'https://github.com/ipcrm/ipcrm-demo_cis.git',
#  :ref => '4e6b63b577b9beae5a3cea88237faf11cb5990a0'

#mod 'rgbank',
#  :git => 'https://github.com/ipcrm/puppetlabs-rgbank.git',
#  :ref => 'master'

#mod 'jenkins',
#  :git => 'https://github.com/jenkinsci/puppet-jenkins.git',
#  :ref => '5ab2c8a8207f6351f59706255087c6eef32778d9'

#mod 'netstat',
#  :git => 'https://github.com/ipcrm/ipcrm-netstat.git',
#  :ref => 'master'

mod 'awskit',
 :git => 'https://github.com/puppetlabs-seteam/awskit.git',
 :ref => 'provision_task'
