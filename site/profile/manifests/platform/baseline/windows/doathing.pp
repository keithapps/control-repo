class profile::platform::baseline::windows::doathing {
  include chocolatey

  Package {
    provider => chocolatey
  }

  package { '7Zip': ensure => latest }
  package { 'jre8': ensure => '8.0.151' }


}
