class profile::platform::baseline::windows::common {

  reboot{'dsc_reboot':
    when    => pending,
    timeout => 15,
  }

  # Disable Windows Defender for demo-speed
  registry_key { 'HKLM\SOFTWARE\Policies\Microsoft\Windows Defender':
    ensure => present,
    before => Registry_value['DisableAntiSpyware'],
  }
  registry_value { 'DisableAntiSpyware':
    ensure => present,
    path   => 'HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\DisableAntiSpyware',
    type   => dword,
    data   => 1,
  }
  reboot { 'afterdisableAntiSpyware':
    subscribe => Registry_value['DisableAntiSpyware'],
  }

}
