class profile::platform::baseline::linux::sudo {
  class { 'sudo': }

  sudo::conf { 'admins':
    priority => 10,
    content  => "%admins ALL=(ALL) NOPASSWD: ALL
  Defaults:%admins !requiretty",
  }

  sudo::conf { 'centos':
    priority => 20,
    content  => "centos ALL=(ALL) NOPASSWD: ALL
  Defaults:centos !requiretty",
  }

  sudo::conf { 'distelli':
    priority => 30,
    content  => "distelli ALL=(ALL) NOPASSWD: ALL
  Defaults:distelli !requiretty",
  }
}
