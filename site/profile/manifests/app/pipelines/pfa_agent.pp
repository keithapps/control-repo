class profile::app::pipelines::pfa_agent (
  String $pfa_access_token = lookup('pfa.accesstoken'),
  String $pfa_secret_key = lookup('pfa.secretkey'),
){
  require ::profile::app::docker
  require ::profile::app::distelli
  case $::kernel {
    'Linux': {
      class { '::distelli::agent':
        access_token => Sensitive($pfa_access_token),
        secret_key   => Sensitive($pfa_secret_key),
        endpoint     => 'pfa.puppetize2018.tsedemos.com:8080',
      }
    }
    'windows': {
      class { '::distelli::agent':
        access_token       => Sensitive($pfa_access_token),
        secret_key         => Sensitive($pfa_secret_key),
        install_chocolatey => true,
        endpoint           => 'pfa.puppetize2018.tsedemos.com:8080',
      }
    }
    default: { fail('Unsupported OS') }
  }
}
