# pfa All in One
class profile::app::pipelines::pfa(){
  require ::profile::app::docker
  docker::run { 'pfa-mysql':
    image                 => 'mysql:5.7',
    env                   => [
      'MYSQL_ROOT_PASSWORD=puppetlabs',
      'MYSQL_DATABASE=pfa',
      'MYSQL_PASSWORD=puppetlabs',
      'MYSQL_USER=pfa'
    ],
    ports                 => '3306:3306',
    volumes               => 'pfa_mysql:/var/lib/mysql',
    before                => Docker::Run['pfa'],
    health_check_interval => 10,
  }
  docker::run { 'pfa':
    image                 => 'puppet/pipelines-for-applications:latest',
    env                   => [
      'DB_ENDPOINT=mysql://pfa-mysql:3306/pfa',
      'USER=pfa',
      'MYSQL_PWD=puppetlabs',
      'DUMP_URI=dump://localhost:7000',
      'PFI_SECRET_KEY=5pM51Fu502mkPN3eKrHbvg=='
    ],
    links                 => [
      'pfa-mysql:pfa-mysql',
      'pfa-artifactory:pfa-artifactory'
    ],
    ports                 => [
      '8080:8080',
      '8000:8000',
      '7000:7000',
    ],
    health_check_interval => 10,
  }
  docker::run { 'pfa-artifactory' :
    image                 => 'docker.bintray.io/jfrog/artifactory-oss:5.8.3',
    ports                 => [
      '8081:8081',
    ],
    health_check_interval => 10,
    volumes               => [
      'pfa_artifactory:/var/opt/jfrog/artifactory',
    ]
  }
}
