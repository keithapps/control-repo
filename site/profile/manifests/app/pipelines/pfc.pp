# pfc All in One
class profile::app::pipelines::pfc(){
  require ::profile::app::docker
  docker::run { 'pfc-mysql':
    image                 => 'mysql:5.7',
    env                   => [
      'MYSQL_ROOT_PASSWORD=puppetlabs',
      'MYSQL_DATABASE=pfc',
      'MYSQL_PASSWORD=puppetlabs',
      'MYSQL_USER=pfc'
    ],
    ports                 => '3306:3306',
    volumes               => 'pfc_mysql:/var/lib/mysql',
    before                => Docker::Run['pfc'],
    health_check_interval => 10,
  }
  docker::run { 'pfc':
    image                 => 'puppet/pipelines-for-containers:latest',
    env                   => [
      'DB_ENDPOINT=mysql://pfc-mysql:3306/pfc',
      'USER=pfc',
      'MYSQL_PWD=puppetlabs',
      'DUMP_URI=dump://localhost:7000',
      'PFI_SECRET_KEY=5pM51Fu502mkPN3eKrHbvg=='
    ],
    links                 => [
      'pfc-mysql:pfc-mysql',
      'pfc-artifactory:pfc-artifactory'
    ],
    ports                 => [
      '8080:8080',
      '8000:8000',
      '7000:7000',
    ],
    health_check_interval => 10,
  }
  docker::run { 'pfc-artifactory' :
    image                 => 'docker.bintray.io/jfrog/artifactory-oss:5.8.3',
    ports                 => [
      '8081:8081',
    ],
    health_check_interval => 10,
    volumes               => [
      'pfc_artifactory:/var/opt/jfrog/artifactory',
    ]
  }
}
