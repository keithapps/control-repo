class profile::app::pipelines::pfc_agent (
  String $pfc_access_token = lookup('pfc.accesstoken'),
  String $pfc_secret_key = lookup('pfc.secretkey'),
){
  require ::profile::app::docker
  require ::profile::app::distelli
  class { '::distelli::agent':
    access_token => Sensitive($pfc_access_token),
    secret_key   => Sensitive($pfc_secret_key),
  }
}
