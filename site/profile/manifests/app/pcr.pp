# pcr All in One
class profile::app::pcr(){
  require ::profile::app::docker
  docker::run { 'pcr-mysql':
    image                 => 'mysql:5.7',
    env                   => [
      'MYSQL_ROOT_PASSWORD=puppetlabs',
      'MYSQL_DATABASE=pcr',
      'MYSQL_PASSWORD=puppetlabs',
      'MYSQL_USER=pcr'
    ],
    ports                 => '3306:3306',
    volumes               => 'pcr-mysql:/var/lib/mysql',
    before                => Docker::Run['pcr'],
    health_check_interval => 10,
  }
  docker::run { 'pcr':
    image                 => 'distelli/Puppet Container Registry:latest',
    env                   => [
      'EUROPA_DB_ENDPOINT=mysql://pcr-mysql:3306/pcr ',
      'EUROPA_DB_USER=pcr',
      'MEUROPA_DB_PASS=password'
    ],
    links                 => [
      'pcr-mysql:pcr-mysql'
    ],
    ports                 => [
      '8080:80',
      '8443:443',
    ],
    health_check_interval => 10,
  }
}
