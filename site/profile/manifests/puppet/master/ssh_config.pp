# Class: ssh_config
#
#
class profile::puppet::master::ssh_config {
  file { '/etc/ssh/ssh_config':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    content => epp('profile/ssh_config.epp'),
  }
}
