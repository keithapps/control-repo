# class profile::puppet::agent
# Upgrades Puppet agent when outdated
class profile::puppet::agent {
  $agentversion = lookup('puppet_agent_version')

  $mac = $facts['ec2_metadata']['mac']
  case $facts['ec2_metadata']['network']['interfaces']['macs'][$mac]['subnet-id'] {
    'subnet-e3711ca8': {
      host { 'ip-10-4-218-244.puppetdemos.net':
        ensure       => 'absent',
      }
      -> host { 'puppet.puppetize2018.puppet.vm':
        ensure       => 'present',
        ip           => '10.4.218.244',
        host_aliases => ['puppet', 'ip-10-4-218-244.puppetdemos.net']
      }
    }
    default:           {
      host { 'ip-10-4-218-244.puppetdemos.net':
        ensure       => 'absent',
      }
      -> host { 'puppet.puppetize2018.puppet.vm':
        ensure       => 'present',
        ip           => '54.203.127.120',
        host_aliases => ['puppet', 'ip-10-4-218-244.puppetdemos.net']
      }
    }
  }

  unless $facts['aio_agent_version']>=$agentversion {
    class { 'puppet_agent':
      package_version => $agentversion,
      require         => Host['ip-10-4-218-244.puppetdemos.net'],
    }
  }
}
