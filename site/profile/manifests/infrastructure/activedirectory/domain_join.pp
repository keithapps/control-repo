# Class: profile::infrastructure::activedirectory::domain_join
#
# Joins a node to a Windows Active Directory domain
class profile::infrastructure::activedirectory::domain_join {
  $ad2join      = lookup('activedirectory.dnsname')
  $joinaccount  = lookup('activedirectory.joinaccount')
  $joinpassword = lookup('activedirectory.joinpassword')
  $arrcertname  = split($trusted['certname'], '[.]')
  $query        = "inventory[facts] { facts.trusted.certname ~ 'activedirectory' }"
  $response     = puppetdb_query($query)
  $dcip         = $response[0]['facts']['ipaddress']

  dsc_xdnsserveraddress { 'dnsserveraddress':
    dsc_address        => $dcip,
    dsc_interfacealias => $facts['networking']['primary'],
    dsc_addressfamily  => 'ipv4',
    before             => Dsc_xComputer['JoinDomain']
  }

  dsc_xComputer { 'JoinDomain':
    dsc_name       => $arrcertname[0],
    dsc_domainname => $ad2join,
    dsc_credential => {
      'user'     => $joinaccount,
      'password' => Sensitive($joinpassword)
    }
  }
}
