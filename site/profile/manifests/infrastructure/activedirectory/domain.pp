# Class: profile::infrastructure::activedirectory::domain
# Builds a Windows domain controller and provisions AD resources
#
class profile::infrastructure::activedirectory::domain(
  $dn           = lookup('activedirectory.dn'),
  $localadminpw = lookup('activedirectory.localadminpw'),
  $domainname   = lookup('activedirectory.dnsname'),
  $domainnbname = lookup('activedirectory.name'),
  $ntdspath     = lookup('activedirectory.ntdspath'),
  $safemodepw   = lookup('activedirectory.safemodepw'),
){

  # resources
  user {'Administrator':
    ensure   => present,
    password => Sensitive($localadminpw)
  }

  file { 'Active Directory NTDS':
    ensure => directory,
    path   => $ntdspath,
  }

  dsc_xdnsserveraddress { 'dnsserveraddress':
    dsc_address        => [ '127.0.0.1', '10.4.0.2' ],
    dsc_interfacealias => $facts['networking']['primary'],
    dsc_addressfamily  => 'ipv4',
  }

  windowsfeature { 'Active Directory Domain Services':
    ensure                 => present,
    name                   => 'AD-Domain-Services',
    installmanagementtools => true,
  }

  dsc_xADDomain { $domainname:
    dsc_domainname                    => $domainname,
    dsc_domainadministratorcredential => {
      'user'     => 'Administrator',
      'password' => Sensitive($localadminpw)
    },
    dsc_safemodeadministratorpassword => {
      'user'     => 'safemode',
      'password' => Sensitive($safemodepw)
    },
    dsc_databasepath                  => $ntdspath,
    dsc_logpath                       => $ntdspath,
    require                           => [
      User['Administrator'],
      File['Active Directory NTDS'],
      Windowsfeature['Active Directory Domain Services'],
      Dsc_xdnsserveraddress['dnsserveraddress']
    ]
  }

  if "${domainnbname}\\" in $facts['id'] {
    dsc_xadorganizationalunit { 'OU_Puppetize2018':
      dsc_ensure => 'Present',
      dsc_name   => 'Puppetize2018',
      dsc_path   => $dn,
      require    => Dsc_xADDomain[$domainname]
    }

    dsc_xaduser {'ADUser_Puppetize2018_1':
      dsc_ensure      => 'Present',
      dsc_domainname  => $domainname,
      dsc_username    => 'Puppetize2018_1',
      dsc_description => 'Puppetize2018 User 1',
      dsc_path        => join(['OU=Puppetize2018', $dn],','),
      dsc_password    => {
        'user'     => 'Puppetize2018_1',
        'password' => Sensitive('@Puppetize2018')
      },
      require         => Dsc_xadorganizationalunit['OU_Puppetize2018'],
    }

    dsc_xadgroup { 'ADGroup_Puppetize2018_Users':
      dsc_ensure           => 'Present',
      dsc_groupname        => 'Puppetize2018_Users',
      dsc_path             => join(['OU=Puppetize2018', $dn],','),
      dsc_memberstoinclude => 'Puppetize2018_1',
      require              => [
        Dsc_xadorganizationalunit['OU_Puppetize2018'],
        Dsc_xaduser['ADUser_Puppetize2018_1']
      ]
    }
  }

}
