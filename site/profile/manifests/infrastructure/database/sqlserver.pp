# Class: profile::infrastructure::database::sql_server
# Installs SQL Server including SQL Management Studio 17.9
#
class profile::infrastructure::database::sql_server (
  $iso           = lookup('sql.install.iso'),
  $iso_drive     = lookup('sql.install.iso_drive'),
  $dotnet_pkg    = lookup('sql.install.dotnet_pkg'),
  $sql_version   = lookup('sql.install.sql_version'),
  $sql_name      = lookup('sql.install.sql_name'),
  $domainname    = lookup('activedirectory.name'),
) {

  $iso_source    = "puppet:///win_repo/${iso}"
  $iso_path      = "C:/Windows/Temp/${iso}"
  $dotnet_source = "puppet:///win_repo/${dotnet_pkg}"
  $dotnet_path   = "C:/Windows/Temp/${dotnet_pkg}"
  $computername  = upcase($facts['hostname'])

  group { 'SQL Admins':
    ensure  => 'present',
    members => ["${computername}\\Administrator"] #"${domainname}\\Domain Admins"
  }

  # Only perform the prep work if SQL is not yet installed
  unless $facts['sqlserver_instances'][$sql_version][$sql_name] {
    file { $dotnet_path:
      ensure => file,
      source => $dotnet_source,
      before => WindowsFeature['Net-Framework-Core']
    }

    file { $iso_path:
      ensure => file,
      source => $iso_source,
    }

    mount_iso { $iso_path :
      drive_letter => $iso_drive,
      require      => File[$iso_path],
      before       => [
        Sqlserver_instance[$sql_name],
        Sqlserver_features['Shared Features']
      ]
    }
  }

  windowsfeature { 'Net-Framework-Core':
    ensure => present,
    source => 'C:\Windows\Temp',
  }

  sqlserver_instance { $sql_name:
    #--------NOT USED----------------------
    #security_mode         => 'SQL',
    #sa_pwd                => 'p@ssw0rd!!',
    #--------NOT USED----------------------
    source                => "${iso_drive}:/",
    features              => ['DQ', 'FullText', 'Replication', 'SQLEngine'],
    sql_sysadmin_accounts => ["${computername}\\SQL Admins", "NT AUTHORITY\\SYSTEM"],
    install_switches      => {
        'TCPENABLED'          => 1,
        'SQLBACKUPDIR'        => 'C:\\MSSQLSERVER\\backupdir',
        'SQLTEMPDBDIR'        => 'C:\\MSSQLSERVER\\tempdbdir',
        'INSTALLSQLDATADIR'   => 'C:\\MSSQLSERVER\\datadir',
        'INSTANCEDIR'         => 'C:\\Program Files\\Microsoft SQL Server',
        'INSTALLSHAREDDIR'    => 'C:\\Program Files\\Microsoft SQL Server',
        'INSTALLSHAREDWOWDIR' => 'C:\\Program Files (x86)\\Microsoft SQL Server',
    },
    require               => [
      WindowsFeature['Net-Framework-Core'],
      Group['SQL Admins']
    ]
  }

  sqlserver_features { 'Shared Features':
    source   => "${iso_drive}:/",
    features => ['IS'],
    require  => [
      WindowsFeature['Net-Framework-Core']
    ]
  }

  file { 'C:/Windows/Temp/SSMS-Setup-ENU.exe':
    ensure => file,
    source => 'puppet:///win_repo/SSMS-Setup-ENU.exe'
  }

  package { 'Microsoft SQL Server Management Studio - 17.9':
    ensure          => installed,
    source          => 'C:/Windows/Temp/SSMS-Setup-ENU.exe',
    install_options => [ '/install', '/quiet', '/norestart' ],
    require         => [
      WindowsFeature['Net-Framework-Core'],
      File['C:/Windows/Temp/SSMS-Setup-ENU.exe']
    ]
  }

  windows_firewall::exception { 'SQL Browser':
    ensure       => present,
    direction    => 'in',
    action       => 'allow',
    enabled      => true,
    protocol     => 'UDP',
    local_port   => 1434,
    remote_port  => 'any',
    display_name => 'SQL Server Browser UDP In',
    description  => 'MS SQL Server Browser Inbound Access, enabled by Puppet [UDP 1434]',
  }

  windows_firewall::exception { 'SQL Server - Default Instance':
    ensure       => present,
    direction    => 'in',
    action       => 'allow',
    enabled      => true,
    protocol     => 'TCP',
    local_port   => 1433,
    remote_port  => 'any',
    display_name => 'SQL Server Default Instance TCP In',
    description  => 'MS SQL Server - Default Instance Inbound Access, enabled by Puppet [TCP 1433]',
  }

  #---------------------------------
  # Begin Hiera-based configuration  
  #---------------------------------
  $sql_config = lookup('sql', Hash, 'hash')

  #Set custom SQL configuration
  sqlserver::config { $sql_name:
    admin_login_type => $sql_config['config']['admin_login_type']
  }

  #Create databases
  $sql_config['databases'].each |$name, $attribs| {
    if $name != '_default'{
      if $attribs != undef {
        sqlserver::database{
          default:
            *       => $sql_config['databases']['_default']
            ;
          $name:
            * => $attribs
        }
      }
      else {
        sqlserver::database{
          default:
            *       => $sql_config['databases']['_default']
            ;
          $name:
        }
      }
    }
  }

  #Create SQL Logins
  $sql_config['logins'].each |$name, $attribs| {
    if $name != '_default'{
      if $attribs != undef {
        sqlserver::login{
          default:
            *       => $sql_config['logins']['_default']
            ;
          $name:
            * => $attribs
        }
      }
      else {
        sqlserver::login{
          default:
            *       => $sql_config['logins']['_default']
            ;
          $name:
        }
      }
    }
  }

  #Create SQL Users and Permissions
  $sql_config['users'].each |$name, $attribs| {
    if $name != '_default'{
      if $attribs['attribs'] != undef {
        sqlserver::user{
          default:
            *       => $sql_config['users']['_default']
            ;
          $name:
            user     => $attribs['user'],
            database => $attribs['database'],
            instance => $attribs['instance'],
            login    => $attribs['dependsonLogin'],
            *        => $attribs['attribs'],
            require  => Sqlserver::Login[$attribs['dependsonLogin']]
        }
      }
      else {
        sqlserver::user{
          default:
            *       => $sql_config['users']['_default']
            ;
          $name:
            user     => $attribs['user'],
            database => $attribs['database'],
            instance => $attribs['instance'],
            login    => $attribs['dependsonLogin'],
            require  => Sqlserver::Login[$attribs['dependsonLogin']]
        }
      }
      $attribs['permissions'].each |$type, $permissions| {
        sqlserver::user::permissions{
          "${type}-${name}-on-${attribs['database']}":
            user        => $attribs['user'],
            database    => $attribs['database'],
            instance    => $attribs['instance'],
            permissions => $permissions,
            state       => $type,
            require     => Sqlserver::User[$name],
        }
      }
    }
  }

  #---------------------------------
  # End Hiera-based configuration  
  #---------------------------------
}
